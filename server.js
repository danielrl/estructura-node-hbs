'use strict'

const express = require('express');
const hbs = require('hbs');
const path = require('path');

require('./config/config');

const app = express();


app.set('port', process.env.PORT || 3000);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(require('./router/index'));

hbs.registerPartials(__dirname + 'views');
app.set('view engine', 'hbs');

app.listen(app.get('port'), () => {
    console.log(`Escuchando puerto ${app.get('port')}`);
})